/**
 * Created by NigamGoyal on 1/3/2020.
 */

public with sharing class ContactManager {


    @AuraEnabled(Cacheable = true)
    public static String getSObjectRecordsWithFieldSet(String recordId, String childObjNameAndFieldSetName) {

        if (String.isNotEmpty(recordId) && String.isNotEmpty(childObjNameAndFieldSetName)) {

            String[] childObjAndFieldSetsArray = childObjNameAndFieldSetName.split(':');
            String objectApiName;
            String fieldSetName;
            if (childObjAndFieldSetsArray.size() >= 2) {
                objectApiName = childObjAndFieldSetsArray[0].trim();
                fieldSetName = childObjAndFieldSetsArray[1].trim();
                List<Schema.FieldSetMember> fieldSetMembers =
                        Schema.getGlobalDescribe().get(objectApiName).getDescribe().fieldSets.getMap().get(fieldSetName).getFields();
                String fieldSet = '';
                for (Schema.FieldSetMember fieldSetMember : fieldSetMembers) {
                    fieldSet += fieldSetMember.fieldPath + ',';
                }
                fieldSet = fieldSet.removeEnd(',');
                String query = 'SELECT ' + fieldSet;
                query += ' FROM ' + objectApiName;
                //  query += ' WHERE AccountId = :recordId';
                List<SObject> recordList = Database.query(query);

                SObjectRecordsAndFieldSetWrapper sobjectRecordsAndFieldSetWrapperObj = new SObjectRecordsAndFieldSetWrapper();
                sobjectRecordsAndFieldSetWrapperObj.fieldSetMembers = fieldSetMembers;
                sobjectRecordsAndFieldSetWrapperObj.records = recordList;
                return JSON.serialize(sobjectRecordsAndFieldSetWrapperObj);
            }
        }
        return null;
    }

    public class SObjectRecordsAndFieldSetWrapper {
        public List<Schema.FieldSetMember> fieldSetMembers;
        public List<SObject> records;
    }


}