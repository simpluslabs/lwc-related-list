/**
 * Created by NigamGoyal on 1/9/2020.
 */

global class FieldSetPicklist extends VisualEditor.DynamicPickList{

    VisualEditor.DesignTimePageContext context;

    global FieldSetPicklist(VisualEditor.DesignTimePageContext context) {
        this.context = context;
    }

    global override VisualEditor.DataRow getDefaultValue(){
//        VisualEditor.DataRow defaultValue = new VisualEditor.DataRow('red', 'RED');
        return null;
    }
    global override VisualEditor.DynamicPickListRows getValues(){
        Set<String> childObjectNameList = new Set<String>();

        Schema.DescribeSObjectResult describeSObjectResult = Schema.getGlobalDescribe().get(this.context.entityName).getDescribe();
        VisualEditor.DynamicPickListRows  dynamicPickListRows = new VisualEditor.DynamicPickListRows();
        for (Schema.ChildRelationship childRelationshipObjectName : describeSObjectResult.getChildRelationships()) {

            String childObjectName = String.valueOf(childRelationshipObjectName.getChildSObject());

            if(!childObjectNameList.contains(childObjectName)){
                childObjectNameList.add(childObjectName);
                Set<String> fieldSetList = new Set<String>();
                if(Schema.getGlobalDescribe().get(childObjectName) != null &&
                        Schema.getGlobalDescribe().get(childObjectName).getDescribe() != null &&
                        Schema.getGlobalDescribe().get(childObjectName).getDescribe().fieldSets != null){
                    fieldSetList = Schema.getGlobalDescribe().get(childObjectName).getDescribe().fieldSets.getMap().keySet();

                    for(String fieldSetName : fieldSetList){
                        String childObjectAndFieldSet_Name = childObjectName + ': ' + fieldSetName;
                        VisualEditor.DataRow dataRow = new VisualEditor.DataRow(childObjectAndFieldSet_Name, childObjectAndFieldSet_Name);
                        dynamicPickListRows.addRow(dataRow);
                    }
                }
            }
        }
        return dynamicPickListRows;
    }
}