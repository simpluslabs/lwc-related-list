/**
 * Created by NigamGoyal on 1/22/2020.
 */

public with sharing class LookUpController {


    @AuraEnabled
    public static SObjectListAndObjLabel getSelectedRecordsList(String sObjectName,
            String showFieldApiName,
            String idFieldApiName,
            List<String> selectedRecordIds,
            List<String> queryFieldsApiName) {

        try {

            SObjectListAndObjLabel sObjectListAndLabelObj = new SObjectListAndObjLabel();

            if(String.isNotEmpty(sObjectName)){
                sObjectListAndLabelObj.label = Schema.getGlobalDescribe().
                        get(sObjectName).
                        getDescribe().getLabel();
            }

            if (String.isNotEmpty(sObjectName) &&
                    String.isNotEmpty(showFieldApiName) &&
                    String.isNotEmpty(idFieldApiName) &&
                    selectedRecordIds != null &&
                    selectedRecordIds.size() > 0) {


                sObjectListAndLabelObj.sObjectList = new List<SObject>();
                String query = 'SELECT ' + showFieldApiName + ' ';
                query += ' , ' + idFieldApiName + ' ';
                for(String sObjectField : queryFieldsApiName){
                    if(sObjectField.toLowerCase() != idFieldApiName.toLowerCase() && sObjectField.toLowerCase() != showFieldApiName.toLowerCase())
                    query += ' , ' + sObjectField + ' ';
                }
                query += ' FROM ' + sObjectName ;
                query += ' WHERE ' + idFieldApiName + ' IN :selectedRecordIds';
                query += ' AND ' + idFieldApiName + '<> NULL ';
                sObjectListAndLabelObj.sObjectList = Database.query(query);

            }
            return sObjectListAndLabelObj;
        } catch (Exception e) {
            String message = '*Error :' + '\n' +
                    e.getLineNumber() + '\n' +
                    e.getMessage() + '\n' +
                    e.getCause() + '\n' +
                    e.getStackTraceString();
            System.debug(message);
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static List<SObject> getRecordsList(String sObjectName,
            String showFieldApiName,
            String idFieldApiName,
            String searchFieldApiName,
            String searchText,
            List<String> selectedRecordIds,
            String whereConditions,
            List<String> queryFieldsApiName) {

        try {
            List<SObject> sObjectList = new List<SObject>();

            if (String.isNotEmpty(sObjectName) &&
                    String.isNotEmpty(searchText) &&
                    String.isNotEmpty(showFieldApiName) &&
                    String.isNotEmpty(idFieldApiName)){

                String searchValue = '\'%' + String.escapeSingleQuotes(searchText.trim()) + '%\'';

                String query = 'SELECT ' + showFieldApiName + ' ';
                query += ' , ' + idFieldApiName + ' ';
                for(String sObjectField : queryFieldsApiName){
                    if(sObjectField.toLowerCase() != idFieldApiName.toLowerCase() && sObjectField.toLowerCase() != showFieldApiName.toLowerCase())
                        query += ' , ' + sObjectField + ' ';
                }
                query += ' FROM ' + sObjectName ;
                query += ' WHERE ' + searchFieldApiName + ' LIKE ' + searchValue;
                query += ' AND ' + idFieldApiName + '<> NULL ';

                if (selectedRecordIds != null && selectedRecordIds.size() > 0)
                    query += ' AND ' + idFieldApiName + ' NOT IN :selectedRecordIds';

                if (String.isNotEmpty(whereConditions))
                    query += ' AND ' + whereConditions;


                sObjectList = Database.query(query);
            }
            return sObjectList;
        } catch (Exception e) {
            String message = '*Error :' + '\n' +
                    e.getLineNumber() + '\n' +
                    e.getMessage() + '\n' +
                    e.getCause() + '\n' +
                    e.getStackTraceString();
            System.debug(message);
            throw new AuraHandledException(e.getMessage());
        }
    }

    public class SObjectListAndObjLabel{
        @AuraEnabled public String label;
        @AuraEnabled public List<SObject> sObjectList;
    }

}