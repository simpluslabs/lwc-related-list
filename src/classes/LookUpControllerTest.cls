/**
 * Created by NigamGoyal on 1/27/2020.
 */

@IsTest
private class LookUpControllerTest {

    @testSetup
    static void testSetup() {
        Account accountObj = new Account();
        accountObj.Name = 'test';
        insert accountObj;

        List<Contact> contactList = new List<Contact>();

        Contact contactObj = new Contact();
        contactObj.LastName = 'Test Contact1';
        contactList.add(contactObj);

        Contact contactObj1 = new Contact();
        contactObj1.LastName = 'Test Contact2';
        contactList.add(contactObj1);

        insert contactList;
    }

    @IsTest
    static void getSelectedRecordsList() {
        Map<Id, Contact> idContactMap = new Map<Id, Contact>([SELECT Id FROM Contact]);
        List<String> contactIds = new List<String>();
        for (Id contactId : idContactMap.keySet()) {
            contactIds.add(contactId);
        }
        LookUpController.getSelectedRecordsList('Contact', 'Name', 'Id', contactIds);
        try {
            LookUpController.getSelectedRecordsList('Contact', 'Nam', 'Id', contactIds);
        } catch (Exception e) {

        }
    }

    @IsTest
    static void getRecordsList() {
        List<Contact> contactList = new List<Contact>();
        contactList = [SELECT Id FROM Contact LIMIT 1];
        List<String> contactIds = new List<String>();

        if (contactList.size() > 0) {
            contactIds.add(contactList[0].Id);
        }
        LookUpController.getRecordsList('Contact', 'Name', 'Id', 'Name', 'Test', contactIds, 'Name = \'Test Contact2\'');

        try {
            LookUpController.getRecordsList('Contact', 'Name', 'I', 'Name', 'Test', contactIds, 'Name = \'Test Contact2\'');
        } catch (Exception e) {
        }
    }
}