/**
 * Created by NigamGoyal on 1/24/2020.
 */

public with sharing class PicklistController {


    @AuraEnabled
    public static Picklist getPicklistValues(String objectApiName, String fieldApiName) {
        try {

            Picklist picklistObj = new Picklist();



            if(String.isNotEmpty(objectApiName) && String.isNotEmpty(fieldApiName)){

                picklistObj.label = Schema.getGlobalDescribe().
                        get(objectApiName).
                        getDescribe().
                        fields.getMap().
                        get(fieldApiName).
                        getDescribe().
                        getLabel();

                picklistObj.picklistValues = new List<PicklistValue>();
                List<Schema.PicklistEntry> picklistEntries = Schema.getGlobalDescribe().
                                                                    get(objectApiName).
                                                                    getDescribe().
                                                                    fields.getMap().
                                                                    get(fieldApiName).
                                                                    getDescribe().
                                                                    getPicklistValues();
                for (Schema.PicklistEntry picklistEntry : picklistEntries) {
                    PicklistValue picklistValueObj = new PicklistValue();
                    picklistValueObj.label = picklistEntry.getLabel();
                    picklistValueObj.value = picklistEntry.getValue();
                    picklistObj.picklistValues.add(picklistValueObj);
                }
            }
            System.debug(picklistObj);
            return picklistObj;
        } catch (Exception e) {
            String message = '*Error :' + '\n' +
                    e.getLineNumber() + '\n' +
                    e.getMessage() + '\n' +
                    e.getCause() + '\n' +
                    e.getStackTraceString();
            System.debug(message);
            throw new AuraHandledException(e.getMessage());
        }
    }

    public class Picklist{
        @AuraEnabled public String label;
        @AuraEnabled public List<PicklistValue> picklistValues;
    }

    public class PicklistValue {
        @AuraEnabled public String label;
        @AuraEnabled public String value;
    }

}