/**
 * Created by NigamGoyal on 5/18/2020.
 */

import { LightningElement, track, api } from 'lwc';

export default class ComSearchPicklist extends LightningElement {

    /************** api properties ***************/
    @api picklistValues;
    @api selectedValue;
    @api isDisabled;
    @api isRequired;

    /************** reactive properties ***************/
    @track tempPicklistValues;
    @track selectedName;
    @track isShowPicklist;

    /************** non-reactive properties ***************/
    isValueChange;
    isShowPicklistFire;

    connectedCallback(){
        this.tempPicklistValues = this.picklistValues;
        this.setSelectedName();
    }

    setSelectedName(){
        let selectedPicklist = this.picklistValues.filter(cur => cur.value === this.selectedValue);
        if(selectedPicklist != null && selectedPicklist.length > 0){
            const name = selectedPicklist[0].Name;
            this.selectedName = selectedPicklist[0].name +'';
        }
    }

    handleHidePicklist(event){
        console.log('handleHidePicklist');
        let self = this;
        setTimeout(function(){
            if(!self.isValueChange){
                self.setSelectedName();
            }
            self.isValueChange = false;
            if(!self.isShowPicklistFire){
                self.isShowPicklist = false;
            }
        },100);
    }
    handleShowPicklist(event){
        this.isShowPicklist = !this.isShowPicklist;
        if(this.isShowPicklist){
            this.handleChangeTempPicklist(event);
        }
    }
    picklistBoxOnMouseDown(event){
        this.isShowPicklistFire = true;
    }
    picklistBoxOnMouseUp(event){
        let self = this;
        console.log('picklistBoxOnMouseUp');
      //  setTimeout(function(){
            self.isShowPicklistFire = false;
        //},100);
    }
    handleChangeTempPicklist(event){
        const value = event.target.value;
        if(value){
            const tempPicklistValues = [];
            this.picklistValues.forEach(cur => {
                if(cur.name.search(value) !== -1){
                    tempPicklistValues.push(cur);
                }
            });
            this.tempPicklistValues = tempPicklistValues;
        }else{
            this.tempPicklistValues = this.picklistValues;
        }
        this.selectedName = value;
        this.isShowPicklist = true;
    }

    handleChangeValue(event){
        let self = this;
        console.log('In comSearchPicklist handleValueChange');
        console.log(event.target.dataset.name);
        console.log(event.target.dataset.value);
        self.isValueChange = true;
        const value = event.target.dataset.value;
        self.selectedName = event.target.dataset.name;
        self.dispatchEvent(new CustomEvent('picklistvaluechange', {
            detail: {
                value: value,
            }
        }));
        self.selectedValue = value;
        self.isShowPicklist = false;
        setTimeout(function(){
            self.handleValidation();
        },100);

    }

    @api
    handleValidation(){
        const allValid = [...this.template.querySelectorAll('lightning-input')]
        .reduce((validSoFar, inputCmp) => {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
        }, true);
        return allValid;
    }

}