/**
 * Created by NigamGoyal on 1/3/2020.
 */

import { LightningElement, track, api, wire } from 'lwc';
import getContactRecordsWithFieldSet from '@salesforce/apex/ContactManager.getContactRecordsWithFieldSet';

export default class ContactRecord extends LightningElement {

    @track fieldSetMembers;
    @track contactList;
    @api recordId;


    @wire(getContactRecordsWithFieldSet,{recordId : '$recordId'})
    wiredContactFieldSetWrapper({error, data}){
        if(data){
            const contactRecords = JSON.parse(data).contacts;
            const fieldSetMembers = JSON.parse(data).fieldSetMembers;
            console.log('contactRecords',contactRecords);
            let contactList = [];
            contactRecords.forEach( cur => {
               let contactRecord = [];
               fieldSetMembers.forEach( curField => {
                  contactRecord.push({value: cur[curField['fieldPath']],
                  key: curField['fieldPath'],
                  type: curField['type'],
                  id: cur['Id']});
               });
               contactList.push(contactRecord);
            });
            this.fieldSetMembers = fieldSetMembers;
            this.contactList = contactList;
            console.log('contactList',contactList);
            this.error = '';
        } else if(error){
            this.fieldSetMembers = '';
            this.contactList = '';
            this.error = error;
        }
    }



}