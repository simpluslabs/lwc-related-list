/**
 * Created by NigamGoyal on 1/3/2020.
 */

import { LightningElement, track, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { refreshApex } from '@salesforce/apex';

import getSObjectRecordsWithFieldSet from '@salesforce/apex/ContactManager.getSObjectRecordsWithFieldSet';

export default class ContactRecordList extends LightningElement {

    @track fieldSetMembers;
    @track showRecordsList;
    @track currentPage = 1;
    @track numberOfPages = 1;
    @track showOptions;
    @track noOfRecordsToDisplay;

    @api showNumberOfRecords = 10; // default value is set through design attribute
    @api recordId;
    @api childObjNameAndFieldSetName;
    @api relatedListName;
    @api childObjectNameList;

    contactList;
    searchList;
    currentSortOrder;

    options = [
            { label: '5', value: '5'},
            { label: '10', value: '10'},
            { label: '20', value: '20'},
            { label: '35', value: '35'},
            { label: '50', value: '50'},
            { label: '75', value: '75'},
            { label: '100', value: '100'},
            { label: '150', value: '150'},
            { label: '200', value: '200'},
        ];

    wiredContactResult;
    @wire(getSObjectRecordsWithFieldSet,{recordId : '$recordId',
       childObjNameAndFieldSetName: '$childObjNameAndFieldSetName'})
    wiredContactFieldSetWrapper(result){
        this.wiredContactResult = result;
        this.refreshData(result);
    }

    refreshData(result){
        if(data){
            this.fieldSetMembers = JSON.parse(data).fieldSetMembers;
            this.recordList = JSON.parse(data).records;
            this.noOfRecordsToDisplay = this.recordList ? this.recordList.length : 0;
             if(!this.recordList || (this.recordList && this.recordList.length === 0)){
                 showToast('Info!', 'info', 'No records found!!!', null, null, this);
             }

            this.searchList = this.recordList;
            if(this.searchList && this.searchList.length > 0){
                this.setNumberOfPagesAndProcessRecords(this.searchList);
                this.performShowOptionAction(this.searchList);
            }

            console.log('recordList',JSON.parse(JSON.stringify(JSON.parse(data).records)));
            this.error = '';
        } else if(error){
            this.fieldSetMembers = [];
            this.recordList = [];
            this.showRecordsList = [];
            this.error = error;
        }
    }

    renderedCallback(){
         let table = this.template.querySelector('.resizeTable');
         resizableGrid(table,this);
    }

    performShowOptionAction(recordList){
        let showOptions = [];
        if(recordList.length <= 5){
            this.showNumberOfRecords = 5;
        }
        for(let i = 0; i < this.options.length; i++){
            let cur = this.options[i];
               showOptions.push(cur);
            if(cur.value > recordList.length){
                if(this.showNumberOfRecords > recordList.length){
                    this.showNumberOfRecords = cur.value;
                }
                break;
            }
        }
        this.showOptions = showOptions;
    }

    handleWrapOrClipTextAction(event){
        const value = event.detail.value;
        let truncateList = this.template.querySelectorAll('.truncate_tr_js');
        let cellIndex;
        if(event &&
            event.currentTarget &&
            event.currentTarget.parentNode){
            cellIndex = event.currentTarget.parentNode.cellIndex;
        }
        console.log(event.currentTarget.parentNode,cellIndex);
        if(cellIndex >= 0){
          if(value === 'ClipText'){
              truncateList.forEach(cur_tr => {
                  let curTableRow = cur_tr.querySelectorAll('.truncate_th_td_js')[cellIndex];
                    console.log('Inside in ClipText',curTableRow);
                  if(curTableRow){
                      console.log('Inside in ClipText');
                      let currDiv = curTableRow.querySelector('.truncate_div_js');
                          currDiv.classList.add('slds-truncate');
                          currDiv.classList.remove('normalWhiteSpace');
                  }
              });
          }else if(value === 'WrapText'){
              truncateList.forEach(cur_tr => {
                  let curTableRow = cur_tr.querySelectorAll('.truncate_th_td_js')[cellIndex];
                    console.log('Inside in wrap Text',curTableRow);
                  if(curTableRow){
                      console.log('Inside in wrap Text');
                      let currDiv = curTableRow.querySelector('.truncate_div_js');
                          currDiv.classList.remove('slds-truncate');
                          currDiv.classList.add('normalWhiteSpace');
                  }
              });
          }
        }
    }
    handleSortOrder(event){

        // hide all asc or desc icon
        hideAllChevron(this.template.querySelectorAll('.chevron'));
        // check data order is asc or desc
        let dataOrder = checkDataOrder(event);
        this.currentSortOrder = event.currentTarget.dataset;

        this.performSortList(this.currentSortOrder);

        this.processRecords(this.currentPage);
//        this.performSortList(event.currentTarget.dataset);
    }

    handleSearchAction(event){
        const searchText = event.target.value;

        if (searchText) {
            this.searchAction(searchText);
        }else{
            this.searchList = this.recordList;
            this.noOfRecordsToDisplay = this.searchList.length;
        }
        this.afterSearchAction();

    }

    searchAction(searchText){
        let searchList = [];
        const regExpSearchText = new RegExp(searchText,"i");
        this.recordList.forEach(cur =>{
            let isSearchTextFind = performSearchAction(cur);
            if(isSearchTextFind !== -1){
                searchList.push(cur);
            }
        });
        function performSearchAction(cur){
            let curObjectValuesArray = Object.values(cur);
            let isSearchTextFind = curObjectValuesArray.findIndex(checkSearchText);
            function checkSearchText(curObjectValue){
                if(typeof(curObjectValue) !== 'object'){
                    curObjectValue = curObjectValue.toString();
                    if(curObjectValue.search(regExpSearchText) != -1){
                        return true;
                    }
                }else{
                    let isSearchTextFind = performSearchAction(curObjectValue);
                    if(isSearchTextFind !== -1){
                        return true;
                    }
                }
            }
            return isSearchTextFind;
        }
        this.searchList = searchList;
        this.noOfRecordsToDisplay = this.searchList ? this.searchList.length : 0;
    }

    afterSearchAction(){
        this.currentPage = 1;
        if(this.currentSortOrder){
            this.performSortList(this.currentSortOrder);
        }
        this.performShowOptionAction(this.searchList);
        this.setNumberOfPagesAndProcessRecords(this.searchList);
    }

    performSortList(dataset){
        let dataOrder = dataset.order;
        let fieldName = dataset.name;
        fieldName = fieldName.split('.');

        let recordList = this.searchList;
        let unSortList = [];
        let sortList = [];
        recordList.forEach(cur => {
            let curObj = JSON.parse(JSON.stringify(cur));
            for(let i = 0; i < fieldName.length; i++){
                if(curObj){
                    curObj = curObj[fieldName[i]];
                }else{
                    break;
                }
            }
            if(curObj){
                sortList.push(cur);
            }else{
                unSortList.push(cur);
            }
        });

        sortList.sort(function(a, b){
            let x = a;
            let y = b;
            for(let i = 0; i < fieldName.length; i++){
                x = x[fieldName[i]];
                y = y[fieldName[i]];
            }
            if(x){
                x = x.toLowerCase();
            }
            if(y){
                y = y.toLowerCase();
            }
            if(dataOrder === 'asc' ){
                if (x < y) {return -1;}
                if (x > y) {return 1;}
            }else{
                if (x > y) {return -1;}
                if (x < y) {return 1;}
            }
            return 0;
          });

          if(dataOrder === 'asc'){
              this.searchList = [...unSortList,...sortList];
          }else{
              this.searchList = [...sortList,...unSortList];
          }
    }

    handleShowNumberOfRecords(event){
        const value = event.detail.value;
        this.currentPage = Math.ceil((((this.currentPage - 1) * this.showNumberOfRecords) + 1) / value);
        this.showNumberOfRecords = value;
        this.setNumberOfPagesAndProcessRecords(this.searchList);

    }

    setNumberOfPagesAndProcessRecords(recordList){
        this.numberOfPages = Math.ceil(recordList.length / this.showNumberOfRecords);
        this.processRecords(this.currentPage);
    }

    handleGoToFirstPage(event){
        this.currentPage = 1;
        this.processRecords(this.currentPage);
    }
    handleGoToPreviousPage(event){
        if(this.currentPage > 1)
        this.processRecords(--this.currentPage);
    }
    handleGoToNextPage(event){
        if(this.currentPage < this.numberOfPages)
        this.processRecords(++this.currentPage);
    }
    handleGoToLastPage(event){
        this.currentPage = this.numberOfPages;
        this.processRecords(this.currentPage);
    }



    processRecords(currPage){
        let noOfPages = this.numberOfPages;
        let showNumberOfRecords = this.showNumberOfRecords;


        if(currPage <= noOfPages && currPage){
            let sliceStart = (currPage - 1) * showNumberOfRecords;
            let sliceEnd = currPage * showNumberOfRecords;
            this.showRecordsList = this.searchList.slice(sliceStart,sliceEnd);
        }else{
            this.showRecordsList = [];
        }

    }

    handleRefreshListAction(event){
        this.refreshUI();
    }

    refreshUI(){
        const self = this;
        const currentPage = self.currentPage;
        self.recordList = [];
        self.searchList = [];
        self.showRecordsList = [];
        refreshApex(self.wiredContactResult).then(() => {
            self.refreshData(self.wiredContactResult);
            self.currentPage = currentPage;
            self.processRecords(currentPage);
        });
    }



}

    function resizableGrid(table,thisArg){

        var row = table.getElementsByTagName('tr')[0],
        cols = row ? row.children : undefined;
        if (!cols) return;

        for (var i=0;i<cols.length;i++){
            var div = createDiv(table.offsetHeight);
            cols[i].appendChild(div);
            cols[i].style.position = 'relative';
            setListeners(div);
        }
        function createDiv(height){
            var div = document.createElement('div');
            var attr = document.createAttribute("c-contactRecordList_contactRecordList");
            div.setAttributeNode(attr);
            div.style.top = 0;
            div.style.right = 0;
            div.style.width = '5px';
            div.style.position = 'absolute';
            div.style.cursor = 'col-resize';

            div.style.userSelect = 'none';
            /* table height */
            div.style.height = height+'px';
            div.className = 'columnSelector';
            return div;
        }

        function setListeners(div){
         var pageX,curCol,nxtCol,curColWidth,nxtColWidth;
         div.addEventListener('mousedown', function (e) {
          curCol = e.target.parentElement;
          nxtCol = curCol.nextElementSibling;
          pageX = e.pageX;
          curColWidth = curCol.offsetWidth
          if (nxtCol)
           nxtColWidth = nxtCol.offsetWidth
         });

         thisArg.template.addEventListener('mousemove', function (e) {
          if (curCol) {
           var diffX = e.pageX - pageX;

           if (nxtCol)
            nxtCol.style.width = (nxtColWidth - (diffX))+'px';

           curCol.style.width = (curColWidth + diffX)+'px';
          }
         });

        thisArg.template.addEventListener('mouseup', function (e) {
         curCol = undefined;
         nxtCol = undefined;
         pageX = undefined;
         nxtColWidth = undefined;
         curColWidth = undefined;
         });
        }
    }

    function hideAllChevron(chevron){

        for(let i = 0; i < chevron.length; i++){
            chevron[i].classList.add('slds-hide');
        }
    }

    function checkDataOrder(event){
        let dataOrder = event.currentTarget.dataset.order;
            if(dataOrder){
                if(dataOrder === 'asc'){
                    dataOrder = 'desc';
                    removeOrShowChevronIconOnUI(event,'slds-hide','slds-show_inline-block');
                }else{
                    dataOrder = 'asc';
                    removeOrShowChevronIconOnUI(event,'slds-show_inline-block','slds-hide');
                }
            }else{
                dataOrder = 'asc';
                removeOrShowChevronIconOnUI(event,'slds-show_inline-block','slds-hide');
            }
            event.currentTarget.dataset.order = dataOrder;
            return dataOrder;
    }

    function removeOrShowChevronIconOnUI(event,chevronUpAdd,chevronUpRemove){
        event.currentTarget.querySelector('.chevronUp').classList.add(chevronUpAdd);
        event.currentTarget.querySelector('.chevronUp').classList.remove(chevronUpRemove)
        event.currentTarget.querySelector('.chevronDown').classList.add(chevronUpRemove);
        event.currentTarget.querySelector('.chevronDown').classList.remove(chevronUpAdd);
    }

    const showToast = function(title, variant, message, mode, duration, thisArg, messageData){
        const event = new ShowToastEvent({
                    'title': title,
                    'variant': variant,
                    'message' : message,
                    'mode' : mode || 'dismissible',
                    'duration': duration || '4000ms',
                    'messageData' : messageData || []
                });
        thisArg.dispatchEvent(event);
    }