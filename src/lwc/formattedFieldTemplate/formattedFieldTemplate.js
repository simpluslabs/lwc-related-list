
/**
 * Created by NigamGoyal on 1/6/2020.
 */

import { LightningElement, api } from 'lwc';

export default class FormattedFieldTemplate extends LightningElement {

    @api objRecord;
    @api fieldSetMember;
    connectedCallback(){

        let path = this.fieldSetMember.fieldPath.split('.');
        let type = this.fieldSetMember.type;
        let value = this.objRecord;
        let fieldApiName;
        let recId;
        for(let i = 0; i < path.length; i++){
            if(value[path[i]]){
                if(path[i] === 'Name' ||
                   path[i] === 'FirstName' ||
                   path[i] === 'LastName'){
                       recId = value.Id;
                       if(recId){
                           fieldApiName = path[i];
                       }
                   }
                value = value[path[i]];
            }else{
                value = '';
                break;
            }
        }
        this.value = value;
        let id = this.objRecord.Id;

        if((type == 'date' ||
           type == 'time' ||
           type == 'datetime') && this.value){
               this.value = new Date(this.value).getTime();
           }

        switch(type) {
          case 'string':
            this.isText = true;
            if(fieldApiName === 'Name' ||
               fieldApiName === 'FirstName' ||
               fieldApiName === 'LastName'){
                this.isText = false;
                this.isRichText = true;
                this.value = `<a href="/${recId}" target="_blank">${this.value}</a>`;
            }
            break;
          case 'double':
            this.isNumber = true;
            break;
          case 'currency':
            this.isCurrency = true;
            break;
          case 'date':
            this.isDate = true;
            break;
          case 'phone':
            this.isPhone = true;
            break;
          case 'email':
            this.isEmail = true;
            break;
          case 'textarea':
            this.isRichText = true;
            break;
          case 'datetime':
            this.isDateTime = true;
            break;
          case 'time':
            this.isTime = true;
            break;
          case 'url':
            this.isURL = true;
            break;
          case 'richtext':
            this.isRichText = true;
            break;
          case 'address':
            this.isAddress = true;
            break;
          case 'geolocation':
            this.isGeoLocation = true;
            break;
          case 'boolean':
            this.isBoolean = true;
            break;
          default:
            this.isText = true;
        }
    }

}