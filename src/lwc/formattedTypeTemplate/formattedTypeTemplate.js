
/**
 * Created by NigamGoyal on 1/6/2020.
 */

import { LightningElement, api } from 'lwc';

export default class FormattedTypeTemplate extends LightningElement {

    @api fieldObj;
    connectedCallback(){

        let type = this.fieldObj.type;
        this.value = this.fieldObj.value;
        let id = this.fieldObj.id;
        let path = this.fieldObj.key;

        if((type == 'date' ||
           type == 'time' ||
           type == 'datetime') && this.value){
               this.value = new Date(this.value).getTime();
           }

        switch(type) {
          case 'string':
            this.isText = true;
            if(path == 'Name'){
                this.isText = false;
                this.isRichText = true;
                this.value = `<a href="/${id}" target="_blank">${this.value}</a>`;
            }
            break;
          case 'double':
            this.isNumber = true;
            break;
          case 'currency':
            this.isCurrency = true;
            break;
          case 'date':
            this.isDate = true;
            break;
          case 'phone':
            this.isPhone = true;
            break;
          case 'email':
            this.isEmail = true;
            break;
          case 'textarea':
            this.isRichText = true;
            break;
          case 'datetime':
            this.isDateTime = true;
            break;
          case 'time':
            this.isTime = true;
            break;
          case 'url':
            this.isURL = true;
            break;
          case 'richtext':
            this.isRichText = true;
            break;
          case 'address':
            this.isAddress = true;
            break;
          case 'geolocation':
            this.isGeoLocation = true;
            break;
          case 'boolean':
            this.isBoolean = true;
            break;
          default:
            this.isText = true;
        }
    }

}
