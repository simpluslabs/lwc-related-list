/**
 * Created by NigamGoyal on 1/22/2020.
 */

import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';


/********************* APEX Methods *********************/

import getSelectedRecordsList from '@salesforce/apex/LookUpController.getSelectedRecordsList';
import getRecordsList from '@salesforce/apex/LookUpController.getRecordsList';


export default class ComLookUp extends LightningElement {

    /********** @tracks ***********/
    @track sObjectList = [];
    @track selectedSObjectList = [];
    @track searchText;
    @track showSearchInputBox = true;
    @track setCssOnPills;
    @track setCssOnMultipleSelectTrue;
    @track setCssOnInputSearchBox;
    @track setCssOnShowListDiv = "slds-is-absolute";

    /********** @apis ***********/
    @api sObjectName; // For eg: sObjectName = 'Contact';
    @api labelFor; // For eg: labelFor = 'Contact';
    @api lookupIcon; // For eg: lookupIcon = 'standard:contact';
    @api showFieldApiName; // For eg: showFieldApiName = 'Name';
    @api idFieldApiName; // For eg: idFieldApiName = 'Id';
    @api searchFieldApiName; // For eg: searchFieldApiName = 'LastName';
    @api whereConditions; // For eg: whereConditions = "FirstName = 'Andy'";
    @api selectedRecordIds = []; // For eg: selectedRecordIds = ['',''] or
                                 //(selectedRecordIds = '' --> this is for multiple Select equal to false);
    @api multipleSelect = false; // For eg: multipleSelect = false --> if user want to be select multiple record
                                 // then it will set multiple select = true
    @api labelHidden = false; // to show label or not on Ui.
    @api queryFieldApiNamesStr; // api name of query fields

    /********** private non-reactive instance ***********/
    queryFieldsApiName = [];
    salesforceObjectList;
    selectedSalesforceSobject;
    isSelectRecordActionCall;

    connectedCallback(){
        try{
            if(!this.lookupIcon){
                this.lookupIcon = 'standard:record';
            }

            // Set Boolean value in this.multipleSelect if user passed string value
            this.multipleSelect = this.setBooleanValue(this.multipleSelect);

            // Set Boolean value in this.labelHidden if user passed string value
            this.labelHidden = this.setBooleanValue(this.labelHidden);

            // Add CSS
            this.setCss();

            // if user set multiple select is true and pass single id and then it convert into array
            // if user set multiple select is false and pass array of ids and then it will show error
            this.setSelectedRecordIdsAndVerifyIdsSize();
            if(this.queryFieldApiNamesStr)
            this.queryFieldsApiName = this.queryFieldApiNamesStr.split(',');

            /* Fetch pre-selected records and show on the UI*/
            this.getPreSelectedRecords();

        } catch (err) {
            console.error(err);
            let error = err.name + ': ' + err.message;
            showToast('Error!', 'error', error, null, null, this);
        }
    }

   handleHidePicklist(event){
        const self = this;
        try{
            console.log('In Handle hide picklist');
            setTimeout(function(){
                if(!self.isSelectRecordActionCall){
                    self.sObjectList = [];
                }
                self.isSelectRecordActionCall = false;

            },200);
        } catch (err) {
            console.error(err);
            let error = err.name + ': ' + err.message;
            showToast('Error!', 'error', error, null, null, this);
        }
    }


    handleSearchTextChangeAction(event){
        /* Fetch records based on the search text*/
        try{
            if(event &&
                event.target &&
                event.target.value &&
                this.sObjectName &&
                this.showFieldApiName &&
                this.idFieldApiName &&
                this.searchFieldApiName){
                    this.searchText = event.target.value;
                    getRecordsList({
                        sObjectName: this.sObjectName,
                        showFieldApiName: this.showFieldApiName,
                        idFieldApiName: this.idFieldApiName,
                        searchFieldApiName: this.searchFieldApiName,
                        searchText: this.searchText,
                        selectedRecordIds : this.selectedRecordIds,
                        whereConditions : this.whereConditions,
                        queryFieldsApiName : this.queryFieldsApiName
                    }).then((response) => {
                        try{
                             this.salesforceObjectList = response;
                            let sObjectList = [];
                            response.forEach(cur => {
                                sObjectList.push(this.setSObject(cur));
                            });
                            this.sObjectList = sObjectList;

                        } catch (err) {
                            let error = err.name + ': ' + err.message;
                            console.error(error);
                            showToast('Error!', 'error', error, null, null, this);
                        }
                    }).catch(error => {
                        if (error && error.statusText && error.body && error.body.message) {
                            let message =`${error.statusText}: ${error.body.message}`;
                            showToast('Error!', 'error', message, null, null, this);
                        } else {
                            let message = 'Unknown error';
                            showToast('Error!', 'error', message, null, null, this);
                        }
                    });
            }else{
                this.sObjectList = [];
            }
        } catch (err) {
            let error = err.name + ': ' + err.message;
            console.error(error);
            showToast('Error!', 'error', error, null, null, this);
        }
    }

    handleOnSelectRecordAction(event){
        /* Add the selected record in the selected-sObject-list records instance*/
        try{
            this.isSelectRecordActionCall = true;
            if(event &&
                event.currentTarget &&
                event.currentTarget.dataset &&
                event.currentTarget.dataset.id &&
                event.currentTarget.dataset.name){
                    let sObject = {Id:'',Name:''};
                    sObject.Id = event.currentTarget.dataset.id;
                    sObject.Name = event.currentTarget.dataset.name;
                    let selectedSObjList = JSON.parse(JSON.stringify(this.selectedSObjectList));
                    selectedSObjList.push(sObject);
                    this.selectedSObjectList = selectedSObjList;
                    let selectedRecordIds;
                    if(this.selectedRecordIds){
                        selectedRecordIds = JSON.parse(JSON.stringify(this.selectedRecordIds));
                    }
                    if(this.multipleSelect){
                        if(!selectedRecordIds){
                            selectedRecordIds = [];
                        }
                        selectedRecordIds.push(sObject.Id);
                    }else{
                        selectedRecordIds = [sObject.Id];
                        this.selectedSalesforceSobject = this.salesforceObjectList.filter(salesforceObj => {
                            return salesforceObj.Id == sObject.Id;
                       });
                        this.showSearchInputBox = false;
                    }
                    this.selectedRecordIds = selectedRecordIds;

                    this.sObjectList = [];
                    this.searchText = null;
                   fireSelectedRecordIdsChangeEvent(this.selectedRecordIds,this);
                }
        } catch (err) {
            let error = err.name + ': ' + err.message;
            console.error(error);
            showToast('Error!', 'error', error, null, null, this);
        }

    }

    handleRemoveSelectedRec(event){
        /*Remove the selected record from the selected-sObject-list records*/
        try{
            if(event &&
            event.currentTarget &&
            event.currentTarget.name){
                const currRecordId = event.currentTarget.name;
                this.selectedSObjectList = this.selectedSObjectList.filter( cur => cur.Id != currRecordId);
                if(this.multipleSelect)
                    this.selectedRecordIds = this.selectedRecordIds.filter( cur => cur != currRecordId);
                else{
                    this.showSearchInputBox = true;
                    this.selectedRecordIds = [];
                    this.selectedSalesforceSobject = [];
                }

                fireSelectedRecordIdsChangeEvent(this.selectedRecordIds,this);
            }
        } catch (err) {
            let error = err.name + ': ' + err.message;
            console.error(error);
            showToast('Error!', 'error', error, null, null, this);
        }
    }

    setSObject(cur){
        try{
            let sObject = {Id:'',Name:''};
            sObject.Id = cur[this.idFieldApiName];
            sObject.Name = cur[this.showFieldApiName];
            return sObject;
        } catch (err) {
            let error = err.name + ': ' + err.message;
            console.error(error);
            showToast('Error!', 'error', error, null, null, this);
        }
    }

// Set Boolean value in this.multipleSelect if user passed string value
    setBooleanValue(element){

        if(typeof element == 'string'){
            if(element && element.toLowerCase()  == 'true')
                element = true
            else if(element && element.toLowerCase()  == 'false')
                element = false
            else if(element){
                let message = 'You have passed the wrong ' + element + ' value.';
                message += ' Please pass either true or false in boolean or string format.';
                showToast('Error!', 'error', message, null, '6000ms', this);
                this.showSearchInputBox = false;
                return;
            }
        }
        return element;
    }

    // Set Css
    setCss(){
        var node = document.createElement("style");
            node.type="text/css";
        if(this.multipleSelect){
            this.setCssOnMultipleSelectTrue = 'slds-show_inline-block';
            this.setCssOnInputSearchBox = 'slds-p-horizontal_xxx-small slds-p-top_xxx-small';
            this.setCssOnPills = "custom-block";
            node.innerHTML=".custom-block[c-lookUp_lookUp] .slds-pill__action{padding: 5px 27px;}";
            node.innerHTML+=" .custom-block[c-lookUp_lookUp] .slds-pill__icon_container{ padding-left: 2px;}";
        }else{
            // add custom css for lightning pill container so that its work properly in multiple select mode false.
            this.setCssOnPills = "custom-block slds-show";
            this.setCssOnShowListDiv = "slds-is-absolute setRightProperty "

            node.innerHTML=".custom-block.slds-show[c-lookUp_lookUp] .slds-pill__action{ display:block; padding: 5px 27px;}";
            node.innerHTML+=" .custom-block.slds-show[c-lookUp_lookUp] .slds-pill__icon_container{ padding-left: 2px;}";
        }
            document.getElementsByTagName("head")[0].appendChild(node);
    }

    // if user set multiple select is true and pass single id and then it convert into array
    // if user set multiple select is false and pass array of ids and then it will show error
    setSelectedRecordIdsAndVerifyIdsSize(){
        if(this.multipleSelect){
            if(this.selectedRecordIds && !Array.isArray(this.selectedRecordIds)){
                this.selectedRecordIds = [this.selectedRecordIds];
            }
        }else{
            if(this.selectedRecordIds){
                if(Array.isArray(this.selectedRecordIds)){
                    this.selectedRecordIds = JSON.parse(JSON.stringify(this.selectedRecordIds));
                    if(this.selectedRecordIds.length > 0){
                         let message = 'You have set multipleSelect equal to false and passing multiple record Ids.';
                            showToast('Error!', 'error', message, null, '6000ms', this);
                            this.showSearchInputBox = false;
                            return;
                    }
                }else{
                    this.selectedRecordIds = [this.selectedRecordIds];
                }
            }
        }
    }

    /* Fetch pre-selected records and show on the UI*/
    @api
    getPreSelectedRecords(){

        if(this.sObjectName){
               let selectedRecordIds;
               if(this.selectedRecordIds){
                    if(Array.isArray(this.selectedRecordIds)){
                        selectedRecordIds = JSON.parse(JSON.stringify(this.selectedRecordIds));
                    }else{
                        selectedRecordIds = [this.selectedRecordIds];
                    }
                }


               getSelectedRecordsList({
                   sObjectName: this.sObjectName,
                   showFieldApiName: this.showFieldApiName,
                   idFieldApiName: this.idFieldApiName,
                   selectedRecordIds : selectedRecordIds,
                   queryFieldsApiName : this.queryFieldsApiName
               }).then((response) => {
                   try{
                       if(response.label && !this.labelFor){
                           this.labelFor = response.label;
                       }
                       if(response.sObjectList && response.sObjectList.length > 0){
                           this.salesforceObjectList = response.sObjectList;

                           let selectedSObjectList = [];
                           response.sObjectList.forEach(cur => {
                               selectedSObjectList.push(this.setSObject(cur));
                           });
                           this.selectedSObjectList = selectedSObjectList;
                           if(!this.multipleSelect){
                               this.showSearchInputBox = false;
                           }
                       }
                   }
                   catch (err) {
                       let error = err.name + ': ' + err.message;
                       console.error(error);
                       showToast('Error!', 'error', error, null, null, this);
                   }
               }).catch(error => {
                   if (error && error.statusText && error.body && error.body.message) {
                       let message =`${error.statusText}: ${error.body.message}`;
                       console.error(message);
                       showToast('Error!', 'error', message, null, null, this);
                   } else {
                       let message = 'Unknown error';
                       console.error(message);
                       showToast('Error!', 'error', message, null, null, this);
                   }
               });
           }
    }



}

const showToast = (title,variant,message,mode,duration,thisArg) =>{
    const event = new ShowToastEvent({
                'title': title,
                'variant': variant,
                'message' : message,
                'mode' : mode || 'dismissible',
                'duration' : duration || '3000ms'
            });
    thisArg.dispatchEvent(event);
}

const fireSelectedRecordIdsChangeEvent = (selectedRecordIds, thisArg) =>{


    try{
        if(thisArg.multipleSelect == false){
            selectedRecordIds = selectedRecordIds[0];
            var selectedSalesforceSobject = thisArg.selectedSalesforceSobject[0];
        }
        thisArg.dispatchEvent(new CustomEvent('selectedrecordidlistchange', {
            detail: {
                selectedRecordIds: selectedRecordIds,
                selectedSalesforceSobject: selectedSalesforceSobject
            }
        }));

    }
    catch (err) {
        let error = err.name + ': ' + err.message;
        console.error(error);
        showToast('Error!', 'error', error, null, null, this);
    }
};


/********** Parent Component JavaScript Code ***********/
/*

import { ShowToastEvent } from 'lightning/platformShowToastEvent';

@api selectedRecordIds = ['0032v00002n0HHlAAM','0032v00002n0HHnAAM'];

handleSelectedRecordIdListChangeAction(event){
    try {
        this.selectedRecordIds = event.detail.selectedRecordIds;
        console.log('selectedRecordIds',this.selectedRecordIds);
    } catch (err) {
        let error = err.name + ': ' + err.message;
        console.error(error);
        showToast('Error!', 'error', error, null, null, this);
    }
}

const showToast = (title,variant,message,mode,duration,thisArg) =>{
    const event = new ShowToastEvent({
                'title': title,
                'variant': variant,
                'message' : message,
                'mode' : mode || 'dismissible',
                'duration' : duration || '3000ms'
            });
    thisArg.dispatchEvent(event);
}*/