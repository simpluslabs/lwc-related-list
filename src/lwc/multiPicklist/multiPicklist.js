/**
 * Created by NigamGoyal on 1/24/2020.
 */

import { LightningElement, track, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

/********************* APEX Methods *********************/

import getPicklistValues from '@salesforce/apex/PicklistController.getPicklistValues';

export default class ComMultiPicklist extends LightningElement {

    /********** @tracks ***********/

    @track options = [];
    @track picklistValues = [];
    @track labelFor; // For eg: labelFor = 'Type';
    @track setCssOnPills;

    /********** @apis ***********/
    @api sObjectName; // For eg: sObjectName = 'Account';
    @api sObjectFieldName; // For eg: sObjectFieldName = 'Type';
    @api preSelectedValues; // For eg: preSelectedValues = 'Prospect;Customer - Direct';
    @api labelHidden = false; // to show label or not on Ui.
    @api selectedValues = [];
    connectedCallback(){
        try{

        var node = document.createElement("style");
            node.type="text/css";
            this.setCssOnPills = "min-width bg-white custom-block";
            node.innerHTML=".custom-block[c-multiPicklist_multiPicklist] .slds-pill__action{padding: 5px 18px 5px 5px;}";
            node.innerHTML+=" .custom-block[c-multiPicklist_multiPicklist] .slds-pill__icon_container{ padding-left: 2px;}";
            document.getElementsByTagName("head")[0].appendChild(node);

             // Set Boolean value in this.labelHidden if user passed string value
            this.labelHidden = this.setBooleanValue(this.labelHidden);

            if(this.preSelectedValues){
                this.preSelectedValues = this.preSelectedValues.split(';');
            }else{
                this.preSelectedValues = [];
            }
            if(this.sObjectName && this.sObjectFieldName){
               getPicklistValues({
                   objectApiName: this.sObjectName,
                   fieldApiName: this.sObjectFieldName
               }).then((response) => {
                   try{
                       if(response.label && response.picklistValues){
                           this.labelFor = response.label;
                           this.picklistValues = response.picklistValues;
                           this.selectedValues = this.picklistValues.filter(cur => {
                                for(let i = 0; i < this.preSelectedValues.length; i++){

                                   if(this.preSelectedValues[i] === cur.value){
                                       return true;
                                   }
                               }
                               return false;
                           });
                       }
                   }
                   catch (err) {
                       let error = err.name + ': ' + err.message;
                       console.error(error);
                       showToast('Error!', 'error', error, null, null, this);
                   }
               }).catch(error => {
                   if (error && error.statusText && error.body && error.body.message) {
                       let message =`${error.statusText}: ${error.body.message}`;
                       showToast('Error!', 'error', message, null, null, this);
                   } else {
                       let message = 'Unknown error';
                       showToast('Error!', 'error', message, null, null, this);
                   }
               });
            }
        } catch (err) {
            console.error(err);
            let error = err.name + ': ' + err.message;
            showToast('Error!', 'error', error, null, null, this);
        }
    }
    handleValueSelectAction(event){
        try{
            if(event &&
                event.currentTarget &&
                event.currentTarget.dataset &&
                event.currentTarget.dataset.label &&
                event.currentTarget.dataset.value){
                    let picklistValue = {};
                        picklistValue.label = event.currentTarget.dataset.label;
                        picklistValue.value = event.currentTarget.dataset.value;
                    this.selectedValues.push(picklistValue);
                    this.options = [];
                    fireSelectedRecordIdsChangeEvent(this.selectedValues,this);
                }
        } catch (err) {
            let error = err.name + ': ' + err.message;
            console.error(error);
            showToast('Error!', 'error', error, null, null, this);
        }
    }
    handleRemoveSelectedValueAction(event){
        try{
            if(event &&
            event.currentTarget &&
            event.currentTarget.name){
                const currValue = event.currentTarget.name;
                const currLabel= event.currentTarget.label;
                this.selectedValues = this.selectedValues.filter( cur => cur.value != currValue);
                this.options = [];
                fireSelectedRecordIdsChangeEvent(this.selectedValues,this);
            }
        } catch (err) {
            let error = err.name + ': ' + err.message;
            console.error(error);
            showToast('Error!', 'error', error, null, null, this);
        }
    }
    handleShowValueAction(){
        try{
            if(this.options && this.options.length > 0){
                this.options = [];
            }else{
                /// filter picklistValues List and remove value of selectedValue List
                this.options = this.picklistValues.filter(cur => {
                        for(let i = 0; i < this.selectedValues.length; i++){
                            if(this.selectedValues[i].value === cur.value){
                                return false;
                            }
                        }
                        return true;
                    }
                );

                // Sort List
                this.sortList();

            }
        } catch (err) {
            let error = err.name + ': ' + err.message;
            console.error(error);
            showToast('Error!', 'error', error, null, null, this);
        }
    }

    // Set Boolean value in this.multipleSelect if user passed string value
    setBooleanValue(element){

        if(typeof element == 'string'){
            if(element && element.toLowerCase() == 'true')
                element = true
            else if(element && element.toLowerCase()  == 'false')
                element = false
            else if(element){
                let message = 'You have passed the wrong ' + element + ' value.';
                message += ' Please pass either true or false in boolean or string format.';
                showToast('Error!', 'error', message, null, '6000ms', this);
                this.showSearchInputBox = false;
                return;
            }
        }
        return element;
    }

    sortList(){
          try{
             this.options.sort(function(a, b){
                let x = a.label;
                let y = b.label;

                if (x < y) {return -1;}
                if (x > y) {return 1;}
                return 0;
              });
          } catch (err) {
              let error = err.name + ': ' + err.message;
              console.error(error);
              showToast('Error!', 'error', error, null, null, this);
          }
    }

    get isShowButton(){
        try{
            return this.selectedValues.length != this.picklistValues.length
        } catch (err) {
            let error = err.name + ': ' + err.message;
            console.error(error);
            showToast('Error!', 'error', error, null, null, this);
        }
    }
}

const showToast = (title,variant,message,mode,duration,thisArg) =>{
    const event = new ShowToastEvent({
                'title': title,
                'variant': variant,
                'message' : message,
                'mode' : mode || 'dismissible',
                'duration' : duration || '3000ms'
            });
    thisArg.dispatchEvent(event);
}

const fireSelectedRecordIdsChangeEvent = (selectedValues, thisArg) =>{
    let values = [];
    selectedValues.forEach(cur => values.push(cur.value));
    values = values.join(';');
    thisArg.dispatchEvent(new CustomEvent('selectedvalueschange', {
        detail: {
            fieldApiName : thisArg.sObjectFieldName,
            value: values
        }
    }));
};