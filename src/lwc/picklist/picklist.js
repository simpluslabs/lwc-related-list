/**
 * Created by NigamGoyal on 1/24/2020.
 */

import { LightningElement, track, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

/********************* APEX Methods *********************/

import getPicklistValues from '@salesforce/apex/PicklistController.getPicklistValues';

export default class Picklist extends LightningElement {

    /********** @tracks ***********/

    @track options;
    @track labelFor; // For eg: labelFor = 'Type';

    /********** @apis ***********/
    @api sObjectName; // For eg: sObjectName = 'Account';
    @api sObjectFieldName; //
    @api selectedValue;
    @api labelHidden = false; // to show label or not on Ui.


    connectedCallback(){
        try{

           // Set Boolean value in this.labelHidden if user passed string value
           this.labelHidden = this.setBooleanValue(this.labelHidden);

            if(this.sObjectName && this.sObjectFieldName){
               getPicklistValues({
                   objectApiName: this.sObjectName,
                   fieldApiName: this.sObjectFieldName
               }).then((response) => {
                   try{
                       console.log(response);
                       if(response.label && response.picklistValues){
                              this.labelFor = response.label;
                              this.options = [{'label':'---None---','value':undefined},...response.picklistValues];
                        }
                   }
                   catch (err) {
                       let error = err.name + ': ' + err.message;
                       console.error(error);
                       showToast('Error!', 'error', error, null, null, this);
                   }
               }).catch(error => {
                   if (error && error.statusText && error.body && error.body.message) {
                       let message =`${error.statusText}: ${error.body.message}`;
                       showToast('Error!', 'error', message, null, null, this);
                   } else {
                       let message = 'Unknown error';
                       showToast('Error!', 'error', message, null, null, this);
                   }
               });
            }else{
                this.options = [{'label':'---None---','value':undefined}];
            }
        } catch (err) {
            console.error(err);
            let error = err.name + ': ' + err.message;
            showToast('Error!', 'error', error, null, null, this);
        }
    }
    handleChangeAction(event){
        const value = event.detail.value;
        this.dispatchEvent(new CustomEvent('selectedvaluechange', {
                detail: {
                    fieldApiName : this.sObjectFieldName,
                    value: value
                }
            }));
    }

    // Set Boolean value in this.multipleSelect if user passed string value
    setBooleanValue(element){

        if(typeof element == 'string'){
            if(element && element.toLowerCase() == 'true')
                element = true
            else if(element && element.toLowerCase() == 'false')
                element = false
            else if(element){
                let message = 'You have passed the wrong ' + element + ' value.';
                message += ' Please pass either true or false in boolean or string format.';
                showToast('Error!', 'error', message, null, '6000ms', this);
                this.showSearchInputBox = false;
                return;
            }
        }
        return element;
    }
}

const showToast = (title,variant,message,mode,duration,thisArg) =>{
    const event = new ShowToastEvent({
                'title': title,
                'variant': variant,
                'message' : message,
                'mode' : mode || 'dismissible',
                'duration' : duration || '3000ms'
            });
    thisArg.dispatchEvent(event);
}