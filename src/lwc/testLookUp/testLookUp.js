/**
 * Created by NigamGoyal on 1/22/2020.
 */

import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';


export default class TestLookUp extends LightningElement {


//    @api selectedRecordIds = ['0032v00002n0HHlAAM','0032v00002n0HHnAAM'];
//    @api selectedRecordIds = '0032v00002n0HHlAAM';
    @api multipleSelect = 'false';
    @api fields =[];

    handleSelectedRecordIdListChangeAction(event){
        try {
//			this.selectedRecordIds = event.detail.selectedRecordIds;
			console.log('selectedRecordIds',JSON.parse(JSON.stringify(event.detail)));
//			console.log('selectedSalesforceSobject',JSON.stringify(event.detail.selectedSalesforceSobject));
		} catch (err) {
			let error = err.name + ': ' + err.message;
			console.error(error);
			showToast('Error!', 'error', error, null, this);
		}
    }




}

const showToast = (title,variant,message,mode,thisArg) =>{
    const event = new ShowToastEvent({
                'title': title,
                'variant': variant,
                'message' : message,
                'mode' : mode || 'dismissible'
            });
    thisArg.dispatchEvent(event);
}