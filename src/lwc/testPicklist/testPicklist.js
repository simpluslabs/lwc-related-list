/**
 * Created by NigamGoyal on 1/24/2020.
 */

import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class TestPicklist extends LightningElement {

 handleSelectedValueChangeAction(event){
        try {
			console.log('selected picklist value',event.detail.value);
		} catch (err) {
			let error = err.name + ': ' + err.message;
			console.error(error);
			showToast('Error!', 'error', error, null, this);
		}
    }
}

const showToast = (title,variant,message,mode,thisArg) =>{
    const event = new ShowToastEvent({
                'title': title,
                'variant': variant,
                'message' : message,
                'mode' : mode || 'dismissible'
            });
    thisArg.dispatchEvent(event);
}